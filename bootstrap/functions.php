<?php

use Illuminate\Http\Response;

if (!function_exists('isDebug')) {
    function isDebug(): bool
    {
        return (bool)config('app.debug');
    }
}

if (!function_exists('error')) {
    function error(int $code, array $data = []): Response
    {
        $view = 'errors.error';

        if (view()->exists("errors.{$code}")) {
            $view = "errors.{$code}";
        }

        /** @psalm-suppress PossiblyUndefinedMethod */
        return response()->view($view, $data, $code);
    }
}
