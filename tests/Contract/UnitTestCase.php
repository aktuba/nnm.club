<?php

namespace Tests\Contract;

use Illuminate\Routing\Route;

/**
 * Class UnitTestCase
 * @package Tests
 */
abstract class UnitTestCase extends TestCase
{
    /**
     * @param array $info
     * @return Route
     */
    protected function createRoute(array $info): Route
    {
        if (!is_array($info['methods'])) {
            $info['methods'] = [$info['methods']];
        }

        if (empty($info['action'])) {
            $info['action'] = null;
        }

        $route = new Route($info['methods'], $info['uri'], $info['action']);

        if (!empty($info['prefix'])) {
            $route->prefix($info['prefix']);
        }

        if (!empty($info['middleware'])) {
            $route->middleware($info['middleware']);
        }

        if (!empty($info['domain'])) {
            $route->domain($info['domain']);
        }

        if (!empty($info['name'])) {
            $route->name($info['name']);
        }

        return $route;
    }
}
