<?php

namespace Tests\Contract;

use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Testing\TestResponse;

/**
 * Class ApiTestCase
 * @package Tests
 */
abstract class ApiTestCase extends TestCase
{
    protected const RESPONSE_META_OK = [
        'success' => true,
        'code' => Response::HTTP_OK,
    ];

    protected const RESPONSE_INVALID_METHOD = [
        'meta' => [
            'success' => false,
            'code' => Response::HTTP_NOT_FOUND,
            'error' => 'Invalid request method',
        ],
        'response' => [],
    ];

    /**
     * @param string $method
     * @param string $uri
     * @param array $parameters
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param string|null $content
     * @return TestResponse
     */
    public function call(
        $method,
        $uri,
        $parameters = [],
        $cookies = [],
        $files = [],
        $server = [],
        $content = null
    ): TestResponse {
        /**
         * empty cache user
         * @see \Illuminate\Auth\RequestGuard::user()
         */
        $this->app->get('auth')->forgetGuards();

        return parent::call($method, $uri, $parameters, $cookies, $files, $server, $content);
    }

    /**
     * @param array $userData
     * @return array
     */
    protected function registerUser(array $userData): array
    {
        $response = $this->json('POST', route('api.register'), $userData);
        $this->checkTokenResponse($response, Response::HTTP_OK);

        $json = $response->json();
        $this->assertEquals(self::RESPONSE_META_OK, $json['meta']);
        return $this->getTokenData($json['response']);
    }

    /**
     * @param string $tokenType
     * @param string $token
     * @return array
     */
    protected function userInfo(string $tokenType, string $token): array
    {
        return $this->getUserInfoFromResponse(
            $this->json('GET', route('api.profile.get'), [], ['Authorization' => "{$tokenType} {$token}"])
        );
    }

    /**
     * @param TestResponse $response
     * @return array
     */
    protected function getUserInfoFromResponse(TestResponse $response): array
    {
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'meta' => ['success', 'code'],
                    'response' => ['id', 'name', 'email'],
                ]
            );

        $json = $response->json();
        $this->assertEquals(self::RESPONSE_META_OK, $json['meta']);

        $data = (array)$json['response'];

        $this->assertIsInt($data['id']);
        $this->assertNotEmpty($data['id']);

        $this->assertIsString($data['name']);
        $this->assertNotEmpty($data['name']);

        $this->assertIsString($data['email']);
        $this->assertNotEmpty($data['email']);

        return $data;
    }

    /**
     * @param array $userData
     * @return array
     */
    protected function loginUser(array $userData): array
    {
        $response = $this->json('POST', route('api.login'), $userData);
        $this->checkTokenResponse($response, Response::HTTP_OK);

        $json = $response->json();
        $this->assertEquals(self::RESPONSE_META_OK, $json['meta']);

        return $this->getTokenData($json['response']);
    }

    /**
     * @param string $token
     * @param bool $all
     */
    protected function logout(string $token, bool $all = false): void
    {
        $route = 'api.logout.current';
        if ($all) {
            $route = 'api.logout.all';
        }

        $response = $this->json('POST', route($route), [], ['Authorization' => "Bearer {$token}"]);
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'meta' => ['success', 'code'],
                    'response' => ['current', 'all'],
                ]
            );
    }

    /**
     * @param string|string[] $paths
     */
    protected function removeFilesAndDirectories($paths): void
    {
        if (!is_array($paths)) {
            $paths = [$paths];
        }

        /** @var FilesystemAdapter $filesystem */
        $filesystem = Storage::disk('images');

        foreach ($paths as $path) {
            $filesystem->delete($path);

            $path = dirname($path);
            while (!empty($path) && $filesystem->deleteDir($path)) {
                $path = trim(Str::beforeLast($path, DIRECTORY_SEPARATOR), DIRECTORY_SEPARATOR);
            }
        }
    }

    /**
     * @param TestResponse $response
     * @param int $statusCode
     */
    private function checkTokenResponse(TestResponse $response, int $statusCode): void
    {
        $response
            ->assertStatus($statusCode)
            ->assertJsonStructure(
                [
                    'meta' => ['success', 'code'],
                    'response' => ['token_type', 'access_token'],
                ]
            );
    }

    /**
     * @param array $data
     * @return array
     */
    private function getTokenData(array $data): array
    {
        $this->assertIsString($data['token_type']);
        $this->assertNotEmpty($data['token_type']);

        $this->assertIsString($data['access_token']);
        $this->assertMatchesRegularExpression('~^[0-9]+|[a-zA-Z0-9]{40}$~isuD', $data['access_token']);

        return $data;
    }
}
