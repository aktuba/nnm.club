#!/usr/bin/make
# Makefile readme (ru): <http://linux.yaroslavl.ru/docs/prog/gnu_make_3-79_russian_manual.html>
# Makefile readme (en): <https://www.gnu.org/software/make/manual/html_node/index.html#SEC_Contents>

SHELL = /bin/bash
THIS_MAKEFILE = $(lastword $(MAKEFILE_LIST))
THIS_MAKE = $(MAKE) --file $(THIS_MAKEFILE)
ENVIRONMENT_FILE=$(shell pwd)/.env

.PHONY : help phpstan phpcs psalm phpunit tests
.DEFAULT_GOAL : help

# This will output the help for each task. thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Show this help
	@printf "\033[33m%s:\033[0m\n" 'Available commands'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_-]+:.*?## / {printf "  \033[32m%-18s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

phpcs: ## Execute phpcs tests
	composer phpcs

psalm: ## Execute phpcs tests
	composer psalm

phpstan: ## Execute phpstan tests
	composer phpstan

phpunit: ## Execute phpunit tests
	composer phpunit $(filter-out $@,$(MAKECMDGOALS))

tests: ## Execute application tests
	composer tests
%:      # thanks to chakrit
	@:    # thanks to William Pursell
