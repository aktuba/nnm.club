<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Model
 * @package App\Contracts
 *
 * @property-read Carbon $created_at
 * @property-read Carbon $updated_at
 */
abstract class Model extends BaseModel
{
    use HasFactory;

    /**
     * @var string
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $table;

    /**
     * @var string
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $dateFormat;

    public const SORT_TYPE_ASC = 'asc';
    public const SORT_TYPE_DESC = 'desc';
}
