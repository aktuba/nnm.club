<?php

namespace App\Models\User;

use App\Casts\Yaml;
use App\Enums\User\Sex;
use App\Models\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Laravel\Sanctum\HasApiTokens;

/**
 * Class UserController
 * @package App\Models\User
 *
 * @mixin Builder
 *
 * @property int $id
 * @property int[] $groups
 * @property string $email
 * @property float $balance
 * @property int $plan_id
 * @property string $password_hash
 * @property string $password
 * @property string $password_salt
 * @property bool $is_admin
 * @property string $nickname
 * @property string $slug
 * @property Carbon $date_reg
 * @property Carbon $date_log
 * @property Carbon $date_group
 * @property string $ip
 * @property bool $is_deleted
 * @property bool $is_locked
 * @property Carbon $lock_until
 * @property string $lock_reason
 * @property string $pass_token
 * @property int $friends_count
 * @property int $subscribers_count
 * @property string $time_zone
 * @property int $karma
 * @property int $rating
 * @property array<string, mixed> $theme
 * @property array<string, mixed> $notify_options
 * @property array<string, mixed> $privacy_options
 * @property int $status_id
 * @property string $status_text
 * @property int $forum_posts_count
 * @property string $forum_sign
 * @property int $inviter_id
 * @property int $invites_count
 * @property Carbon $date_invites
 * @property Carbon $birth_date
 * @property int $city
 * @property string $city_cache
 * @property string $hobby
 * @property array<string, string> $avatar
 * @property string $site
 * @property int $land
 * @property string $land_cache
 * @property bool $sex
 * @property string $foto
 * @property string $emailaddress
 * @property string $psevdo
 * @property string $clans
 * @property string $lychka
 * @property string $template
 * @property bool $official
 * @property string $gen
 * @property string $interes
 */
class User extends Model
{
    use HasApiTokens;
    use Notifiable;

    /**
     * @var string
     */
    protected $table = 'cms_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     *
     * @psalm-suppress NonInvariantDocblockPropertyType
     */
    protected $fillable = [
        'email',
        'password',
        'password_hash',
        'password_salt',
        'nickname',
        'slug',
        'date_reg',
        'time_zone',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     *
     * @psalm-suppress NonInvariantDocblockPropertyType
     */
    protected $hidden = [
        'password',
        'password_hash',
        'password_salt',
        'pass_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     *
     * @psalm-suppress NonInvariantDocblockPropertyType
     */
    protected $casts = [
        'plan_id' => 'integer',
        'is_admin' => 'boolean',
        'date_reg' => 'datetime',
        'date_log' => 'datetime',
        'date_group' => 'datetime',
        'is_deleted' => 'boolean',
        'is_locked' => 'boolean',
        'lock_until' => 'datetime',
        'friends_count' => 'integer',
        'subscribers_count' => 'integer',
        'karma' => 'integer',
        'rating' => 'integer',
        'status_id' => 'integer',
        'forum_posts_count' => 'integer',
        'inviter_id' => 'integer',
        'invites_count' => 'integer',
        'date_invites' => 'datetime',
        'birth_date' => 'datetime',
        'city' => 'integer',
        'land' => 'integer',
        'sex' => Sex::class,
        'official' => 'boolean',
        'groups' => Yaml::class,
        'theme' => Yaml::class,
        'notify_options' => Yaml::class,
        'privacy_options' => Yaml::class,
        'avatar' => Yaml::class,
    ];
}
