<?php

namespace App\Http\Requests;

use Closure;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Redirector;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class AbstractRequest
 * @package App\Http\Requests
 */
abstract class Request extends FormRequest
{
    /**
     * @var Container
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $container;

    /**
     * @var SessionInterface|callable(): SessionInterface
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $session;

    /**
     * @var string
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $locale;

    /**
     * @var Closure
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $routeResolver;

    /**
     * @var Closure
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $userResolver;

    /**
     * @var array
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $convertedFiles;

    /**
     * @var Validator
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $validator;

    /**
     * @var string
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $redirectAction;

    /**
     * @var string
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $redirectRoute;

    /**
     * @var string
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $redirect;

    /**
     * @var Redirector
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $redirector;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
}
