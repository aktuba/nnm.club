<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Psr\Log\LogLevel;
use Sentry\EventId;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<Throwable>, LogLevel::*>
     */
    protected $levels = [];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'password',
        'password_hash',
        'password_salt',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register(): void
    {
        /** @var EventId|null $eventId */
        $eventId = null;

        $this->reportable(function (Throwable $throwable) use (&$eventId): void {
            if (app()->bound('sentry')) {
                /** @psalm-suppress PossiblyUndefinedMethod */
                $eventId = app('sentry')->captureException($throwable);
            }
        });

        if (!isDebug()) {
            $this->renderable(function (Throwable $throwable) use (&$eventId): Response {
                return error(SymfonyResponse::HTTP_INTERNAL_SERVER_ERROR, ['eventId' => $eventId]);
            });
        }
    }
}
